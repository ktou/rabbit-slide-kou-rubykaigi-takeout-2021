#!/usr/bin/env ruby

# Rehearsal -----------------------------------------------------
# CSV: Ruby: 0        0.867305   0.043896   0.911201 (  0.911251)
# CSV: Red Arrow: 0   0.168953   0.044898   0.213851 (  0.016073)
# Apache Arrow: 0     0.000396   0.000040   0.000436 (  0.000437)
# CSV: Ruby: 1        1.820866   0.057001   1.877867 (  1.874802)
# CSV: Red Arrow: 1   0.358689   0.055034   0.413723 (  0.021555)
# Apache Arrow: 1     0.000393   0.000028   0.000421 (  0.000421)
# CSV: Ruby: 2        4.141544   0.089525   4.231069 (  4.215467)
# CSV: Red Arrow: 2   0.704372   0.133196   0.837568 (  0.040835)
# Apache Arrow: 2     0.000447   0.000025   0.000472 (  0.000472)
# CSV: Ruby: 3        7.919982   0.188867   8.108849 (  8.082351)
# CSV: Red Arrow: 3   1.352553   0.235089   1.587642 (  0.080943)
# Apache Arrow: 3     0.000544   0.000028   0.000572 (  0.000573)
# CSV: Ruby: 4       20.722920   0.449558  21.172478 ( 21.123315)
# CSV: Red Arrow: 4   2.570883   0.427440   2.998323 (  0.146126)
# Apache Arrow: 4     0.000898   0.000000   0.000898 (  0.000899)
# ------------------------------------------- total: 42.355370sec
#
#                         user     system      total        real
# CSV: Ruby: 0        0.792350   0.104316   0.896666 (  0.828678)
# CSV: Red Arrow: 0   0.173136   0.007682   0.180818 (  0.012443)
# Apache Arrow: 0     0.000574   0.000026   0.000600 (  0.000592)
# CSV: Ruby: 1        1.816272   0.043755   1.860027 (  1.840314)
# CSV: Red Arrow: 1   0.387305   0.058731   0.446036 (  0.021403)
# Apache Arrow: 1     0.000662   0.000031   0.000693 (  0.000685)
# CSV: Ruby: 2        3.755837   0.055756   3.811593 (  3.797536)
# CSV: Red Arrow: 2   0.702905   0.145476   0.848381 (  0.040435)
# Apache Arrow: 2     0.000833   0.000038   0.000871 (  0.000862)
# CSV: Ruby: 3        8.003925   0.227114   8.231039 (  8.205680)
# CSV: Red Arrow: 3   1.250248   0.320944   1.571192 (  0.074629)
# Apache Arrow: 3     0.004287   0.000013   0.004300 (  0.004290)
# CSV: Ruby: 4       19.384487   0.514361  19.898848 ( 19.850910)
# CSV: Red Arrow: 4   2.586117   0.454729   3.040846 (  0.138448)
# Apache Arrow: 4     0.008140   0.000005   0.008145 (  0.008139)

require "benchmark"

require "datasets-arrow"

dataset = Datasets::PostalCodeJapan.new
table = dataset.to_arrow
n = 5
# n.times do |i|
#   table.save("/tmp/codes.#{i}.csv")
#   table.save("/tmp/codes.#{i}.arrow")
#   table = table.concatenate([table])
# end

Benchmark.bmbm do |job|
  n.times do |i|
    job.report("CSV: Ruby: #{i}") do
      CSV.read("/tmp/codes.#{i}.csv").size
    end

    job.report("CSV: Red Arrow: #{i}") do
      Arrow::Table.load("/tmp/codes.#{i}.csv")
    end

    job.report("Apache Arrow: #{i}") do
      Arrow::Table.load("/tmp/codes.#{i}.arrow")
    end
  end
end
