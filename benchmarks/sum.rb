#!/usr/bin/env ruby

# Rehearsal ------------------------------------------------
# Array          0.000759   0.000000   0.000759 (  0.000754)
# Arrow::Array   0.002483   0.000000   0.002483 (  0.021204)
# Numo::Int8     0.000610   0.000000   0.000610 (  0.000610)
# --------------------------------------- total: 0.003852sec
#
#                    user     system      total        real
# Array          0.000763   0.000006   0.000769 (  0.000766)
# Arrow::Array   0.000333   0.000002   0.000335 (  0.000333)
# Numo::Int8     0.000634   0.000004   0.000638 (  0.000636)

require "benchmark"

require "arrow"
require "numo/narray"

array = 1000000.times.to_a
arrow_array = Arrow::Array.new(array)
numo_array = Numo::Int8[array]

Benchmark.bmbm do |job|
  job.report("Array") do
    array.sum
  end

  job.report("Arrow::Array") do
    arrow_array.sum
  end

  job.report("Numo::Int8") do
    numo_array.sum
  end
end
