#!/usr/bin/env ruby

# Rehearsal ------------------------------------------
# Ruby     1.254892   0.031662   1.286554 (  1.286584)
# Arrow    0.002342   0.000113   0.002455 (  0.002430)
# DuckDB   0.459440   0.000000   0.459440 (  0.459448)
# --------------------------------- total: 1.748449sec
#
#              user     system      total        real
# Ruby     1.252897   0.003972   1.256869 (  1.256864)
# Arrow    0.001361   0.000037   0.001398 (  0.001395)
# DuckDB   0.459227   0.000000   0.459227 (  0.459101)

require "benchmark"

require "datasets-arrow"
require "arrow-duckdb"

dataset = Datasets::PostalCodeJapan.new
arrow_dataset = dataset.to_arrow
db = DuckDB::Database.open
connection = db.connect
connection.register("codes", arrow_dataset)

Benchmark.bmbm do |job|
  job.report("Ruby") do
    dataset.find_all do |row|
      row.prefecture == "東京都"
    end
  end

  job.report("Arrow") do
    arrow_dataset.slice do |slicer|
      slicer.prefecture == "東京都"
    end
  end

  job.report("DuckDB") do
    result = connection.query("SELECT * FROM codes WHERE prefecture = ?",
                              "東京都",
                              output: :arrow)
    result.to_table
  end
end
