#!/usr/bin/env ruby

# Rehearsal ----------------------------------------------
# Sequential   4.371320   0.279862   4.651182 (  4.560637)
# Ractor       4.930339   0.494265   5.424604 (  1.454782)
# ------------------------------------ total: 10.075786sec
#
#                  user     system      total        real
# Sequential   4.434695   0.396617   4.831312 (  4.573742)
# Ractor       4.871155   0.518646   5.389801 (  1.454987)

require "benchmark"

require "datasets-arrow"

table = Datasets::PostalCodeJapan.new.to_arrow
Ractor.make_shareable(table)

# Show warning
Ractor.new {}.take

n_ractors = 4
n_jobs_per_ractor = 1000

Benchmark.bmbm do |job|
  job.report("Sequential") do
    (n_ractors * n_jobs_per_ractor).times do
      table.slice do |slicer|
        slicer.prefecture == "東京都"
      end
    end
  end

  job.report("Ractor") do
    ractors = n_ractors.times.collect do
      Ractor.new(table, n_jobs_per_ractor) do |t, n|
        n.times do
          t.slice do |slicer|
            slicer.prefecture == "東京都"
          end
        end
      end
    end
    ractors.each(&:take)
  end
end
