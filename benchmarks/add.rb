#!/usr/bin/env ruby

# Rehearsal ------------------------------------------------
# Array          0.040361   0.004003   0.044364 (  0.044364)
# Arrow::Array   0.004211   0.000001   0.004212 (  0.023220)
# Numo::UInt8    0.000217   0.000001   0.000218 (  0.000217)
# --------------------------------------- total: 0.048794sec
#
#                    user     system      total        real
# Array          0.042996   0.000000   0.042996 (  0.042996)
# Arrow::Array   0.001329   0.000000   0.001329 (  0.001329)
# Numo::UInt8    0.000108   0.000000   0.000108 (  0.000106)

require "benchmark"

require "arrow"
require "numo/narray"

array = 1000000.times.to_a
arrow_array = Arrow::Array.new(array)
numo_array = Numo::UInt8[array]

Benchmark.bmbm do |job|
  job.report("Array") do
    array.collect do |value|
      value + 1
    end
  end

  job.report("Arrow::Array") do
    Arrow::Function.find("add").execute([arrow_array, 1]).value
  end

  job.report("Numo::UInt8") do
    numo_array + 1
  end
end
