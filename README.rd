= Red Arrow - Ruby and Apache Arrow

To use Ruby for data processing widely, Apache Arrow support is important. We can do the followings with Apache Arrow:

  * Super fast large data interchange and processing
  * Reading/writing data in several famous formats such as CSV and Apache Parquet
  * Reading/writing partitioned large data on cloud storage such as Amazon S3

This talk describes the followings:

  * What is Apache Arrow
  * How to use Apache Arrow with Ruby
  * How to integrate with Ruby 3.0 features such as MemoryView and Ractor

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Sutou Kouhei

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-rubykaigi-takeout-2021

=== Show

  rabbit rabbit-slide-kou-rubykaigi-takeout-2021.gem
